{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Model selection \n",
    "\n",
    "### (Machine Learning con Applicazioni,  $\\quad$ C. Barbieri, October 2023)\n",
    "\n",
    "$$ ~~ $$\n",
    "\n",
    "The previous lectures discussed some basing guidelines about constructing proper models to learn some specific problems. Using a statistical approach and the notion of statistical risk, we could define the Bayes' optimal predictor as the one that gives the best possible predictions on an unlimited dataset. Alhough impractical in most cases, Bayes' optimal predictor can give useful hints on how to parameterize a learning model. The principle of maximum likelihood can guide us in defining an efficient cost function.  Moreover, analysing the distribution of points within the dataset, we have seen how regularize the parameter set in order to avoid uncontrolled behavior during the training. Yet, a given machine learning problem could be approached using several learning models: we need a way to *estimate the true error of these models* and to select those that provide us with the *most reliable solutions*.\n",
    "\n",
    "We will now see how to define different types of error and how use them to estimate the true uncertainties of a model. In general, increasing the complexity of models does not necessary imply improvements since too much expressivity can lead to overfitting. In fact the best models will be those with a good balance between the need for **reducing the bias of the model** and the need to **limit its variance** (wich is a tatistical measure of overfitting).  With these tools at hand one can build a systematic approach to select the best models and discuss what to do when the data is scarse or limited.\n",
    "\n",
    "The material discussed in the following is based on Chapter 7 of Hastie et al.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Error definitions\n",
    "\n",
    "Let us start considering a supervised learning problem where the the lables, $y\\in\\mathcal{Y}$, and the characteristics, $\\boldsymbol{x}\\in\\mathcal{X}$, are drown from a common probability $P(Y,\\boldsymbol{X})$. In fact, this probability is what defines our *learning problem*.\n",
    "\n",
    "Practical applications will always be limited to some finite set of model predictors, $\\mathcal{C}$, each depending on a set of parameters $\\{\\theta_i\\}$ that need to be optimized during the learning process:\n",
    "$$\n",
    "  h(\\,\\boldsymbol{x}\\,;\\,\\{\\theta_i\\}, f(\\boldsymbol{x};\\{\\theta_i\\})\\,)  \\in  \\mathcal{C} \\,.\n",
    "$$\n",
    "\n",
    "When training the our machine learning model we will use some loss function $\\ell(y,\\hat{y})$ and will be confronting it with a particular training dataset of $N_\\mathcal{S}$ datapoints drawn from $P(Y,\\boldsymbol{X})$:\n",
    "$$\n",
    "\\mathcal{S} = \\{(y_i,\\boldsymbol{x}_i): \\, y\\in\\mathcal{Y}, \\, \\boldsymbol{x}\\in\\mathcal{X}, \\, i=1,2,\\ldots,\\,N_\\mathcal{S}\\} \\, .\n",
    "$$\n",
    "Note that we are using a different label here ($\\mathcal{S}$ instead of $\\mathcal{D}$) to remark that this may not be the entire set of data at our availability but only the part we use totrain a model. Moreover, it is also possible to have different datasets, for example from separate measurements campaigns, that are used for different independent trainings.\n",
    "\n",
    "Given $\\mathcal{S}$, the best model $\\hat{f}^*(\\boldsymbol{x})\\equiv\\hat{f}(\\boldsymbol{x};\\{\\theta_i^*\\})$ is found by minimising the cost function:\n",
    "$$\n",
    "\\begin{aligned}\n",
    " \\{\\theta^*\\},f^* ={}& \\underset{\\{\\theta\\},f \\in \\mathcal{C}}{\\mathrm{arg\\,min}} \n",
    " \\left\\{ C(\\{\\theta\\})\\right\\} \\\\\n",
    "                ={}& \\underset{\\{\\theta\\},f \\in \\mathcal{C}}{\\mathrm{arg\\,min}} \\left\\{ \\sum_{i=1}^{N_\\mathcal{S}} \\ell(y_i,\\hat{f}(\\boldsymbol{x}_i;\\{\\theta\\}) \\right\\} \\,.\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Once we have found our model $\\hat{f}(\\boldsymbol{x};\\{\\theta_i\\})$ we need to define a measure of its performance on our learning problem. We can define three different errors.\n",
    "\n",
    "The **training error** for a model $\\hat{f}(\\boldsymbol{x};\\{\\theta_i\\})$ is simply the minimal cost function evaluated at the best parameterization, $\\{\\theta^*\\}$:\n",
    "$$\n",
    "\\mathcal{E}_{train}(\\mathcal{S}) = \\frac 1{N_\\mathcal{S}} \\sum_{i=1}^{N_\\mathcal{S}} \\ell(y_i,\\hat{f}(\\boldsymbol{x}_i;\\{\\theta^*\\}) \\,.\n",
    "$$\n",
    "Note that this error depends explicitely on the dataset used to train the model.\n",
    "\n",
    "The training error allows us to optimize our model. It gives us a lower bound of the error we can hope to achieve, given the information contained in $\\mathcal{S}$. However, it relies on the data used to train the model and it does not give us any hint of whether the model could (or could not) work well on new data. In fact, if the model is highly expressive or the set $\\mathcal{S}$ does not cover properly the the domains $\\mathcal{X}$ and $\\mathcal{Y}$ we run tha risk of *overtraining*.\n",
    "\n",
    "To have a proper estimation of the quality of out fitted $\\hat{f}^*(\\boldsymbol{x})$, we need to *generalise* our error by testing it on new data. Ideally, we would like to have a full statistics from the probability $P(Y,\\boldsymbol{X})$ so that prediction for all possible $\\boldsymbol{x}\\in\\boldsymbol{\\mathcal{X}}$ are probed. \n",
    "This leads us to the **generalization error** (a.k.a. **statistical risk**) seen in Notes 3:\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\mathcal{E}_{gen.}(\\hat{f}^*;\\mathcal{S}) = \n",
    "\\mathcal{L}_{P}(\\hat{f}^*) ={}& \\mathbb{E}_{P(Y,\\boldsymbol{X})}[\\ell(Y,\\hat{f}^*(\\boldsymbol{X}))]  \\\\\n",
    "      ={}& \\mathbb{E}_{\\boldsymbol{X}} \\left[ \\;\\mathbb{E}_{Y} [\\ell(Y,\\hat{f}^*(\\boldsymbol{s}))|\\boldsymbol{X}=\\boldsymbol{x}] \\; \\right],\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Note that the generalization error still depends on the training set, $\\mathcal{S}$, through $\\hat{f}^*(\\boldsymbol{x};\\mathcal{S})$. This dependency is in fact as important to gauge the capability for overtrain: if the model  $\\hat{f}^*$ is capable to introduce uncontrolled behaviour upon the training process, it variance upon changes of the training set will be large.  thus we define a **mean preditction error**, or **mean test error**, by averaging over an infinite ensemble of datasets all drawn from $P(Y,\\boldsymbol{X})$:\n",
    "$$\n",
    "\\mathcal{E}_{test}(\\hat{f}) = \\mathbb{E}_{\\mathcal{S}}\\left[\\mathcal{E}_{gen.}(\\mathcal{S})\\right] \\, .\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bias-Variance tradeoff\n",
    "\n",
    "\n",
    "<!--### Bias-variance for regression\n",
    "\n",
    "### Bias-variance for kNN--\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bias-variance for regression\n",
    "\n",
    "### Bias-variance for kNN\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Model selection\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sampling\n",
    "\n",
    "### Cross-validation\n",
    "\n",
    "### Bootstrap\n",
    "\n",
    "### Jackknife\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "10e89b4373ca82b9aa008416dbc6678ec2573a3d463e333e4d350f38af34d33f"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
