# Esami per il corso di "Machine Learning con Applicazioni", edizione autunno 2024

I primi appelli si terranno nelle giornate di mercoledì 29 gennaio, 12 febbraio e 19 febbraio 2025. Queste date sono fissate.

Successivi appelli verrano aperti ad aprile, giugno-luglio, settembre e novembre. Per questi ultimi le date saranno flessibili previa disponibiltà del docente (mandare una mail per accordarsi).

Le linee guida si possono trovare in questa cartella e le modalità d'esame sono sostanzialmente simili agli anni passati. Per il progetto computazionale è necessario scegliere tra quelli suggeriti nelle linee guida.

Cordialmente,
  C. Barbieri,  dicembre 2024.
